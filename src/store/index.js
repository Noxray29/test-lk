import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const initialState = {
    zoo: [
        { id: 0, name: 'ewe', score: 0 },
        { id: 1, name: 'iguana', score: 0 },
        { id: 2, name: 'peccary', score: 0 },
        { id: 3, name: 'sloth', score: 0 },
        { id: 4, name: 'ape', score: 0 },
        { id: 5, name: 'ferret', score: 0 },
        { id: 6, name: 'kangaroo', score: 0 },
        { id: 7, name: 'quagga', score: 0 },
        { id: 8, name: 'starfish', score: 0 },
        { id: 9, name: 'blue_crab', score: 0 },
        { id: 10, name: 'frog', score: 0 },
        { id: 11, name: 'leopard', score: 0 },
        { id: 12, name: 'rat', score: 0 },
        { id: 13, name: 'cheetah', score: 0 },
        { id: 14, name: 'gemsbok', score: 0 },
        { id: 15, name: 'llama', score: 0 },
        { id: 16, name: 'reindeer', score: 0 },
        { id: 17, name: 'dog', score: 0 },
        { id: 18, name: 'ground_hog', score: 0 },
        { id: 19, name: 'moose', score: 0 },
        { id: 20, name: 'silver_fox', score: 0 }
    ],
}

const state = { ...initialState }

export const mutations = {
    SET_WINNER (state, winner) {
        state.zoo.find(animal => animal.name === winner.name).score++
    }
}

export const actions = {
    // Here Actions
}

const getters = {
    zoo: state => state.zoo
}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
